package algorithms;

import java.util.ArrayList;
import graph.*;

/**
 * Created by marce on 19.05.2017.
 */
public class UPGMA {

    ArrayList<String> species = new ArrayList<>();
    ArrayList<ArrayList<Double>> distance_matrix = new ArrayList<>();

    String path = "C:/Users/marce/Documents/PhyloTree/2_small.txt";

    public int sizeof_distance_matrix() {
        return this.distance_matrix.size();
    }

    public ArrayList<String> getSpecies() {
        return this.species;
    }

    public String getSpeciesByID(int id) {
        return this.species.get(id);
    }

    public void parseDistMatrix(String input) {
        String currentLine;
        String[] input_lines = input.split("\n");
        boolean firstLine = true;

        for (String line : input_lines) {

            if (firstLine == false) {
                String[] lineSplit = line.split("\t");

                ArrayList<Double> tempList = new ArrayList<>();
                for (int i = 0; i < lineSplit.length; i++) {
                    if (i == 0) {
                        species.add(lineSplit[i]);
                    } else {
                        tempList.add(Double.parseDouble(lineSplit[i]));
                    }
                }
                distance_matrix.add(tempList);

            } else {
                firstLine = false;
            }
        }
    }

    public int[] findMinimum() {
        double minValue = 0;
        int[] coords = new  int[2];
        for (int i = 0; i < distance_matrix.size(); i++) {
            for (int j = 0; j < distance_matrix.get(i).size(); j++) {
                if (minValue == 0) {
                    minValue = distance_matrix.get(i).get(j);
                    coords[0] = i;
                    coords[1] = j;
                } else {
                    if (distance_matrix.get(i).get(j) < minValue) {
                        minValue = distance_matrix.get(i).get(j);
                        coords[0] = i;
                        coords[1] = j;
                    }
                }
            }
        }
    return coords;
    }

    public void merge(int i, int j, Graph graph) {
        String newSpecies = species.get(i) + species.get(j);
        species.add(newSpecies);
        distance_matrix.add(new ArrayList<Double>());

        double weight;
        if (i > j) {
            weight = distance_matrix.get(i).get(j) / 2;
        } else {
            weight = distance_matrix.get(j).get(i) / 2;
        }

        Node newNode = new Node(newSpecies);

        Node child_i = graph.getNodeByName(species.get(i));
        Node child_j = graph.getNodeByName(species.get(j));

        Edge newEdge_i = new Edge(newNode, child_i, weight);
        Edge newEdge_j = new Edge(newNode, child_j, weight);

        newNode.addEdgeOut(newEdge_i);
        newNode.addEdgeOut(newEdge_j);

        child_i.setEdgeIn(newEdge_i);
        child_j.setEdgeIn(newEdge_j);

        graph.addEdge(newEdge_i);
        graph.addEdge(newEdge_j);

        graph.addNode(newNode);

        for (int k = 0; k < distance_matrix.size() - 1; k++) {
            if (k != i && k != j) {
                double i_value, j_value;
                if (k < i) {
                    i_value = distance_matrix.get(i).get(k);
                } else {
                    i_value = distance_matrix.get(k).get(i);
                }

                if (k < j) {
                    j_value = distance_matrix.get(j).get(k);
                } else {
                    j_value = distance_matrix.get(k).get(j);
                }


                distance_matrix.get(distance_matrix.size() - 1).add(((i_value + j_value) / 2));
            } else {
                distance_matrix.get(distance_matrix.size() - 1).add(0.0);
            }
        }

        distance_matrix.remove(i);
        distance_matrix.remove(j);

        species.remove(i);
        species.remove(j);

        for (int k = 0; k < distance_matrix.size(); k++) {
            if (i < j) {
                if (distance_matrix.get(k).size() >= i && distance_matrix.get(k).size() >= j) {
                    distance_matrix.get(k).remove(i);
                    distance_matrix.get(k).remove(j);
                } else {
                    if (distance_matrix.get(k).size() >= i && distance_matrix.get(k).size() < j){
                        distance_matrix.get(k).remove(i);
                    }
                }
            } else {
                if (distance_matrix.get(k).size() >= j && distance_matrix.get(k).size() >= i) {
                    distance_matrix.get(k).remove(i);
                    distance_matrix.get(k).remove(j);
                } else {
                    if (distance_matrix.get(k).size() >= j && distance_matrix.get(k).size() < i){
                        distance_matrix.get(k).remove(j);
                    }
                }
            }
        }
    }

    public void print_distance_matrix() {
        System.out.println("\n");
        for (int i = 0; i < distance_matrix.size(); i++) {
            System.out.println(species.get(i) + " " + distance_matrix.get(i));
        }
    }

    public String generateNewick(Node parent) {

        String temp = "";
        for (Edge edge : parent.getEdges_out()) {
            temp += edge.getHead().getName() + ":" + edge.getWeight() + ",";
        }
        if (!parent.getEdges_out().isEmpty()) {
            temp = "(" + temp.substring(0,temp.length() - 1) + ")" + parent.getName();

            for (Edge edge : parent.getEdges_out()) {
                String replacement = generateNewick(edge.getHead());
                if (!replacement.equals("")) {
                    temp = temp.replaceFirst(edge.getHead().getName(), replacement);
                }
            }
        }
        return temp;
    }
    public static String doUPGMA(String input) {

        Graph graph = new Graph();
        UPGMA upgma= new UPGMA();
        upgma.parseDistMatrix(input);

        for (String s : upgma.getSpecies()) {
            graph.addNode(new Node(s));
        }

        while (upgma.sizeof_distance_matrix() > 1) {
            int[] coords = upgma.findMinimum();
            upgma.merge(coords[0], coords[1], graph);
            //upgma.print_distance_matrix();
        }

        for (Node n : graph.getNodes()) {
            if (n.getEdge_in() == null) {
                graph.setRoot(n);
                break;
            }
        }
        String newick = upgma.generateNewick(graph.getRoot()) + ";";
        System.out.println(newick);

        return newick + "\n" + graph.printASCIITree();
    }
}