package algorithms;

import java.util.ArrayList;
import graph.*;

/**
 * Created by marce on 19.05.2017.
 */
public class NeighbourJoining {

    ArrayList<String> species = new ArrayList<>();
    ArrayList<Double> q_List = new ArrayList<>();
    ArrayList<ArrayList<Double>> distance_matrix = new ArrayList<>();
    ArrayList<ArrayList<Double>> working_matrix = new ArrayList<>();

    String path = "C:/Users/marce/Documents/PhyloTree/2_small.txt";

    public int sizeof_distance_matrix() {
        return this.distance_matrix.size();
    }

    public ArrayList<String> getSpecies() {
        return this.species;
    }

    public ArrayList<Double> getQs() {
        return this.q_List;
    }

    public String getSpeciesByID(int id) {
        return this.species.get(id);
    }

    /*
     * This method reads the distance matrix from a given source stores
     * the distance information in the distance_matrix and the species
     * information in the species list.
     * Usually the input comes from the Input Text field of the GUI.
     */
    public void parseDistMatrix(String input) {
        String currentLine;
        String[] input_lines = input.split("\n");
        boolean firstLine = true;

        for (String line : input_lines) {

            if (firstLine == false) {
                String[] lineSplit = line.split("\t");

                ArrayList<Double> tempList = new ArrayList<>();
                for (int i = 0; i < lineSplit.length; i++) {
                    if (i == 0) {
                        species.add(lineSplit[i]);
                    } else {
                        tempList.add(Double.parseDouble(lineSplit[i]));
                    }
                }
                distance_matrix.add(tempList);
            } else {
                firstLine = false;
            }
        }

        for (ArrayList<Double> line: distance_matrix) {
            line.add(0.0);
        }
    }

    /*
     * This method calculates the average distances from one node
     * to all the others. It sums up the distances to the corresponding
     * species first line-wise and then, after j is greater than i,
     * column-wise (so it reads the values 'around the corner' which
     * is one solution for a lower triangle matrix)
     */
    public void calculate_Qs() {
        double temp;
        q_List.clear();
        for (int i = 0; i < distance_matrix.size(); i++) {
            temp = 0.0;
            for (int j = 0; j < distance_matrix.size(); j++) {
                if (i >= j ) {
                    temp += distance_matrix.get(i).get(j);
                } else {
                    temp += distance_matrix.get(j).get(i);
                }
            }
            temp /= distance_matrix.size() - 2;
            //System.out.println("i: " + i + " " + temp);
            q_List.add(i, temp);
        }
        calculate_working_matrix();
    }

    /*
     * The working matrix is used to determine the two species to be merged
     * next. It is calculated by subtracting the average distances from the
     * corresponding entry in the distance matrix ( K(i,j) - q(i) - q(j) )
     */
    private void calculate_working_matrix() {
        working_matrix.clear();
        for (int i = 0; i < distance_matrix.size(); i++) {
            ArrayList<Double> temp = new ArrayList<Double>();
            for (int j = 0; j < distance_matrix.get(i).size(); j++) {
                if ( i != j) {
                    temp.add(distance_matrix.get(i).get(j) - q_List.get(i) - q_List.get(j));
                } else {
                    temp.add(distance_matrix.get(i).get(j));
                }
            }
            working_matrix.add(temp);
            //System.out.println("DISTANCE MATRIX:\n" + distance_matrix);
            //System.out.println("WORKING MATRIX:\n" + working_matrix);
        }
    }

    /*
     * This method searches a gives matrix for it's minimum value
     * and returns the corresponding line and column number.
     * If multiple entrys share the minimum value, only the
     * line and column number of the first appearence is returned.
     * Reading direction is top-down and left-to-right.
     */
    public int[] findMinimum() {
        double minValue = 0;
        int[] coords = new  int[2];
        for (int i = 0; i < working_matrix.size(); i++) {
            for (int j = 0; j < working_matrix.get(i).size(); j++) {
                if (minValue == 0) {
                    minValue = working_matrix.get(i).get(j);
                    coords[0] = i;
                    coords[1] = j;
                } else {
                    if (working_matrix.get(i).get(j) < minValue) {
                        minValue = working_matrix.get(i).get(j);
                        coords[0] = i;
                        coords[1] = j;
                    }
                }
            }
        }
        return coords;
    }

    /*
     * This method creates a new node and sets the nodes which are given
     * as input and its children. The new node itself is connected to the
     * central node.
     */
    public void merge(int i, int j, Graph graph) {
        // Create a new node by adding it to the species list
        // and providing it with a line in the distance matrix
        String newSpecies = species.get(i) + species.get(j);
        species.add(newSpecies);
        distance_matrix.add(new ArrayList<Double>());

        // The weight of the edges connecting the new node to
        // it's children is half the distance the children had
        // between them, respectively
        double weight_to_i;
        double weight_to_j;
        if (i > j) {
            weight_to_i = (distance_matrix.get(i).get(j) + q_List.get(i) - q_List.get(j)) / 2;
            weight_to_j = distance_matrix.get(i).get(j) - weight_to_i;
        } else {
            weight_to_i = (distance_matrix.get(j).get(i) + q_List.get(i) - q_List.get(j)) / 2;
            weight_to_j = distance_matrix.get(j).get(i) - weight_to_i;
        }

        Node child_i = graph.getNodeByName(species.get(i));
        Node child_j = graph.getNodeByName(species.get(j));
        Node central_node = graph.getNodeByName("Central");

        // Diconnect the children from the central node
        Edge edge_i = null;
        Edge edge_j = null;
        for (Edge e : central_node.getEdges_out()) {
            if (e.getHead() == child_i) {
                edge_i = e;
            } else {
                if (e.getHead() == child_j) {
                    edge_j = e;
                }
            }
        }

        // remove the reference to the edges pointing towards the children
        central_node.getEdges_out().remove(edge_i);
        central_node.getEdges_out().remove(edge_j);

        // remove the edges
        graph.getEdges().remove(edge_i);
        graph.getEdges().remove(edge_j);

        // Create a new node and connect the children via edges
        Node newNode = new Node(newSpecies);

        // create three new edges: two edges between the new node and the children
        // and one between the new node and the central one
        Edge newEdge_i = new Edge(newNode, child_i, weight_to_i);
        Edge newEdge_j = new Edge(newNode, child_j, weight_to_j);

        Edge newEdge_fromCentral = null;

        // Distanz newNode - central = (d(i, central) + d(j, central) - d(ij)) / 2
        if (i < j) {
            newEdge_fromCentral = new Edge(central_node, newNode, (q_List.get(i) + q_List.get(j) - distance_matrix.get(j).get(i)) / 2);
        } else {
            newEdge_fromCentral = new Edge(central_node, newNode, (q_List.get(i) + q_List.get(j) - distance_matrix.get(i).get(j)) / 2);
        }

        // Set the references correctly
        central_node.addEdgeOut(newEdge_fromCentral);

        newNode.setEdgeIn(newEdge_fromCentral);
        newNode.addEdgeOut(newEdge_i);
        newNode.addEdgeOut(newEdge_j);

        child_i.setEdgeIn(newEdge_i);
        child_j.setEdgeIn(newEdge_j);

        // Add the three new edges as well as the new node to the graph
        graph.addNode(newNode);
        graph.addEdge(newEdge_i);
        graph.addEdge(newEdge_j);
        graph.addEdge(newEdge_fromCentral);

        // The new node has distances to all other nodes which are calculated by
        // the arithmetic mean of its children's distances to the other nodes.
        // The k < i and k < j make sure the values are read from the lower
        // triangle matrix, otherwise there would be a NullPointerException
        // The distances from the new node to it's childrens are set to 0.0,
        // but they will be deleted anyway.
        for (int k = 0; k < distance_matrix.size() - 1; k++) {
            if (k != i && k != j) {
                double i_value, j_value;
                if (k < i) {
                    i_value = distance_matrix.get(i).get(k);
                } else {
                    i_value = distance_matrix.get(k).get(i);
                }
                if (k < j) {
                    j_value = distance_matrix.get(j).get(k);
                } else {
                    j_value = distance_matrix.get(k).get(j);
                }

                // Distance from the new node to every other taxa is (d(i, taxon) + d(j, taxon) - d(i,j)) / 2
                if ( i < j ) {
                    distance_matrix.get(distance_matrix.size() - 1).add(((i_value + j_value - distance_matrix.get(j).get(i)) / 2));
                } else {
                    distance_matrix.get(distance_matrix.size() - 1).add(((i_value + j_value - distance_matrix.get(i).get(j)) / 2));
                }

            } else {
                distance_matrix.get(distance_matrix.size() - 1).add(0.0);
            }
        }

        // Add 0.0 at the end of the new line to complete it
        distance_matrix.get(distance_matrix.size() - 1).add(0.0);

        // remove the lines in the distance matrix associated with the children
        distance_matrix.remove(i);
        distance_matrix.remove(j);

        // remove the children from the species list
        species.remove(i);
        species.remove(j);

/*
        for (ArrayList<Double> line : distance_matrix) {
            line.remove(line.size() - 1);
        }
*/
        for (int k = 0; k < distance_matrix.size(); k++) {
            if (i < j) {
                // i and j columns can be removed only if they are really existent within the line
                if (distance_matrix.get(k).size() > i && distance_matrix.get(k).size() > j) {
                    distance_matrix.get(k).remove(i);
                    distance_matrix.get(k).remove(j);
                } else {
                    // maybe the line has a length which includes i, but not j? (i < j is a prerequisite)
                    if (distance_matrix.get(k).size() > i && distance_matrix.get(k).size() <= j){
                        distance_matrix.get(k).remove(i);
                    }
                }
                // the else branch does the same but j <= i
            } else {
                if (distance_matrix.get(k).size() > j && distance_matrix.get(k).size() > i) {
                    distance_matrix.get(k).remove(i);
                    distance_matrix.get(k).remove(j);
                } else {
                    if (distance_matrix.get(k).size() > j && distance_matrix.get(k).size() <= i){
                        distance_matrix.get(k).remove(j);
                    }
                }
            }
        }
    }

    /*
     * The humble method prints the distance matrix on the console.
     */
    public void print_distance_matrix() {
        System.out.println("\n");
        for (int i = 0; i < distance_matrix.size(); i++) {
            System.out.println(species.get(i) + " " + distance_matrix.get(i));
        }
    }

    /*
     * This method generates a Newick tree format (weights included) out of
     * a given structure beneath a given node. If you want the Newick format
     * for the whole graph, the input node of your choice is the root node.
     */
    public String generateNewick(Node parent) {

        String temp = "";
        for (Edge edge : parent.getEdges_out()) {
            temp += edge.getHead().getName() + ":" + edge.getWeight() + ",";
        }
        if (!parent.getEdges_out().isEmpty()) {
            temp = "(" + temp.substring(0,temp.length() - 1) + ")" + parent.getName();

            for (Edge edge : parent.getEdges_out()) {
                String replacement = generateNewick(edge.getHead());
                if (!replacement.equals("")) {
                    temp = temp.replaceFirst(edge.getHead().getName(), replacement);
                }
            }
        }
        return temp;
    }

    /*
     * This method is the core method for running the algorithm.
     */
    public static String doNJ(String input) {

        Graph graph = new Graph();
        NeighbourJoining nj = new NeighbourJoining();
        nj.parseDistMatrix(input);
        nj.calculate_Qs();

        // Prepare the initial star form for Neighbour Joining
        // with a central node and all species directly connected
        Node central_node = new Node("Central");
        graph.addNode(central_node);
        for (int i = 0;  i < nj.getSpecies().size(); i++) {
            Node temp_node = new Node(nj.getSpecies().get(i));
            Edge temp_edge = new Edge(central_node, temp_node, nj.getQs().get(i));
            temp_node.setEdgeIn(temp_edge);
            central_node.addEdgeOut(temp_edge);
            graph.addNode(temp_node);
        }

        while (nj.sizeof_distance_matrix() > 2) {
            nj.calculate_Qs();

            int[] coords = nj.findMinimum();
            nj.merge(coords[0], coords[1], graph);
            nj.print_distance_matrix();
        }

        // Find the graph's root which is the
        // only node with no incoming edges
        for (Node n : graph.getNodes()) {
            if (n.getEdge_in() == null) {
                graph.setRoot(n);
                break;
            }
        }
        String newick = nj.generateNewick(graph.getRoot()) + ";";
        System.out.println(newick);

        //return newick + "\n" + graph.printASCIITree();

        return newick + "\n" + graph.printASCIITree();
    }
}