package gui;

import java.awt.BorderLayout;
import java.awt.Choice;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.HeadlessException;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class GUI extends JFrame {

    /**
     * @param args
     */

    private static final long serialVersionUID = 1L;

    private JPanel Panel_Center = null;
    private JPanel Panel_North = null;
    private JPanel Panel_South = null;
    private JPanel Panel_West = null;
    private JPanel Panel_Ost = null;
    private JPanel Fläche1 = null;
    private JPanel Fläche2 = null;
    private JPanel Fläche3 = null;
    private JPanel Fläche4 = null;

    private JLabel Sequenz = null;
//	private JTextArea Programmhinweis = null;

    private JTextArea Eingabe_TA = null;
    private JTextArea Ausgabe_TA = null;

    private JButton Laden = null;
    private JButton Programm_starten = null;
    private JButton Datei_speichern = null;

    private Choice Auswahlbox = null;

    private JScrollPane Scrollen1 = null;
    private JScrollPane Scrollen2 = null;

    private MausSteuerung steuerung = null;

    public GUI() throws HeadlessException {
        super();
    }


    public void init() {

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Stammbaumrekonstruktion");
        this.setSize(901, 600);
        this.setLaF();

        this.steuerung = new MausSteuerung();
        this.steuerung.setFenster(this);

        BorderLayout steuerung = new BorderLayout();
        this.setLayout(steuerung);

        this.getContentPane().add(getJNorden(), BorderLayout.NORTH);
        this.getContentPane().add(Fläche1(), BorderLayout.WEST);
        this.getContentPane().add(getJVorhersage(), BorderLayout.CENTER);
        this.getContentPane().add(Fläche2(), BorderLayout.EAST);
        this.getContentPane().add(getJSüden(), BorderLayout.SOUTH);

        this.setMinimumSize(new Dimension(640, 480));

    }


    private JPanel Fläche1() {
        if (Panel_West == null) {
            Panel_West = new JPanel();
        }
        return Panel_West;
    }


    private JPanel Fläche2() {
        if (Panel_Ost == null) {
            Panel_Ost = new JPanel();
        }
        return Panel_Ost;
    }


    private JPanel getJNorden() {
        if (Panel_North == null) {
            Panel_North = new JPanel();
            Panel_North.setLayout(new BorderLayout());
//			Panel_North.add(getJTextAreaProgrammhinweis(), BorderLayout.CENTER);
        }
        return Panel_North;
    }

	
	
	/*private JTextArea getJTextAreaProgrammhinweis() {
		if (Programmhinweis == null) {
			Programmhinweis = new JTextArea();
			Programmhinweis.setText("Das Programm ermöglicht eine Vorhersage der Sekundärstruktur für Nukleotidsequenzen und Proteinsequenzen."
							+ " \nDie Vorhersage kann mit dem Chou-Fasman-Algorithmus für Proteinsequenzen oder dem Nussinov-Algorithmus für Nukleotidsequenzen durchgeführt werden.");
			Programmhinweis.setEditable(false);
			Programmhinweis.setBackground(Color.lightGray);
			Programmhinweis.setLineWrap(true);
		}
		return Programmhinweis;
	} */


    private JPanel getJVorhersage() {
        if (Panel_Center == null) {
            Panel_Center = new JPanel();
            Panel_Center.setLayout(new BoxLayout(Panel_Center, BoxLayout.Y_AXIS));

            Panel_Center.add(getJPanelFläch1());

            Panel_Center.add(getPanel2());
            Panel_Center.add(getJPanel3());
            Panel_Center.add(getJPanel4());
        }
        return Panel_Center;
    }


    private JPanel getJPanelFläch1() {
        if(Fläche1 == null){
            Fläche1 = new JPanel();
            Fläche1.setLayout(new FlowLayout());

            Fläche1.add(getJButtonDateiladen());
        }
        return Fläche1;
    }


    private JButton getJButtonDateiladen() {
        if (Laden == null) {
            Laden = new JButton("Datei laden");
            Laden.setBackground(Color.magenta);
            Laden.setActionCommand(MausSteuerung.ACTION_LADEN);
            Laden.addActionListener(steuerung);
        }
        return Laden;
    }


    private JPanel getPanel2() {
        if (Fläche2 == null) {
            Fläche2 = new JPanel();
            Fläche2.setLayout(new BoxLayout(Fläche2, BoxLayout.Y_AXIS));

            Fläche2.add(getJTextAreaEingabe());
            Fläche2.add(getJScrollPane1());
        }
        return Fläche2;
    }


    private JScrollPane getJScrollPane1() {
        if (Scrollen1 == null) {
            Scrollen1 = new JScrollPane(Eingabe_TA);
            Scrollen1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
            Scrollen1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        }
        return Scrollen1;
    }


    public JTextArea getJTextAreaEingabe() {
        if (Eingabe_TA == null) {
            Eingabe_TA = new JTextArea(5, 20);
            Eingabe_TA.setLineWrap(true);
            Eingabe_TA.setFont(new Font("Monospaced", Font.PLAIN, 12));
        }
        return Eingabe_TA;
    }


    private JPanel getJPanel3() {
        if (Fläche3 == null) {
            Fläche3 = new JPanel();
            Fläche3.setLayout(new FlowLayout());

            Fläche3.add(getJLabel());
            Fläche3.add(getChoiceAuswahlbox());
            Fläche3.add(getJButtonProgrammStarten());
        }
        return Fläche3;
    }


    private JLabel getJLabel() {
        if (Sequenz == null) {
            Sequenz = new JLabel("Algorithmus:");
        }
        return Sequenz;
    }


    protected Choice getChoiceAuswahlbox() {
        if (Auswahlbox == null) {
            Auswahlbox = new Choice();
            Auswahlbox.addItem("Neighbor Joining");
            Auswahlbox.addItem("UPGMA");
        }
        return Auswahlbox;
    }


    private JButton getJButtonProgrammStarten() {
        if (Programm_starten == null) {
            Programm_starten = new JButton("Berechnung starten");
            Programm_starten.setBackground(Color.magenta);
            Programm_starten.setActionCommand(MausSteuerung.ACTION_PROGRAMM_STARTEN);
            Programm_starten.addActionListener(steuerung);
        }
        return Programm_starten;
    }

    private JPanel getJPanel4() {
        if (Fläche4 == null) {
            Fläche4 = new JPanel();
            Fläche4.setLayout(new BoxLayout(Fläche4, BoxLayout.Y_AXIS));

            Fläche4.add(getJScrollPane2());
        }
        return Fläche4;
    }


    private Component getJScrollPane2() {
        if (Scrollen2 == null) {
            Scrollen2 = new JScrollPane(getJTextAreaAusgabe());
            Scrollen2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            Scrollen2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        }
        return Scrollen2;
    }


    public JTextArea getJTextAreaAusgabe() {
        if (Ausgabe_TA == null) {
            Ausgabe_TA = new JTextArea(15, 20);
            Ausgabe_TA.setLineWrap(false);
            Ausgabe_TA.setFont(new Font("Monospaced", Font.PLAIN, 12));
        }
        return Ausgabe_TA;
    }


    private JPanel getJSüden() {
        if (Panel_South == null) {
            Panel_South = new JPanel();

            Panel_South.setLayout(new FlowLayout());
            Panel_South.add(getDateispeichern());
        }
        return Panel_South;
    }


    private JButton getDateispeichern() {
        if (Datei_speichern == null) {
            Datei_speichern = new JButton("Datei speichern unter");
            Datei_speichern.setBackground(Color.magenta);
            Datei_speichern.setActionCommand(MausSteuerung.ACTION_DATEI_SPEICHERN);
            Datei_speichern.addActionListener(steuerung);
        }
        return Datei_speichern;
    }


    private void setLaF() {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        GUI fenster = new GUI();
        fenster.init();
        fenster.setVisible(true);
    }

}
