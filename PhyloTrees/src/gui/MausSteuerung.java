package gui;

import algorithms.NeighbourJoining;
import algorithms.UPGMA;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.filechooser.FileNameExtensionFilter;

public class MausSteuerung implements ActionListener {

    public final static String ACTION_LADEN = "Datei laden";
    public static final String ACTION_PROGRAMM_STARTEN = "Programm starten";
    public static final String ACTION_DATEI_SPEICHERN = "Datei speichern";

    private GUI fenster;

    public GUI getFenster() {
        return fenster;
    }

    public void setFenster(GUI fenster) {
        this.fenster = fenster;
    }

    public void actionPerformed(ActionEvent e) {

        JTextArea eingabe = fenster.getJTextAreaEingabe();
        JTextArea ausgabe = fenster.getJTextAreaAusgabe();

        String ac = e.getActionCommand();

        if (ac.equals(ACTION_LADEN)) {


            String seq = readFromFile();
            seq.replaceAll("\\\\n", System.getProperty("line.separator"));
            eingabe.setText(seq);
            ausgabe.setText("");
        }

        if (ac.equals(ACTION_PROGRAMM_STARTEN)) {
            int index = fenster.getChoiceAuswahlbox().getSelectedIndex();
            String input = eingabe.getText();
            if (index == 1) {
                String result = (UPGMA.doUPGMA(input));
                ausgabe.setText(result);
            } else {
                String result = (NeighbourJoining.doNJ(input));
                ausgabe.setText(result);
            }
        }

        if (ac.equals(ACTION_DATEI_SPEICHERN)) {
            saveToFile(ausgabe.getText());
        }

    }

    private String readFromFile() {

        JFileChooser fc = new JFileChooser();
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);

        int opt = fc.showOpenDialog(fenster);
        if (opt == JFileChooser.CANCEL_OPTION) {
            JOptionPane.showMessageDialog(fenster, "Keine Datei ausgewählt",
                    "Dateiauswahl-Abbruch", JOptionPane.ERROR_MESSAGE);
            return "";
        }

        File f = fc.getSelectedFile();
        String line = "";
        String seq = "";
        try {
            FileReader fr = new FileReader(f);
            BufferedReader buf = new BufferedReader(fr);
            while ((line = buf.readLine()) != null) {
                seq += line + "\n";
            }
            buf.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return seq;
    }

    private void saveToFile(String text) {
        // ersetzt UNIX-Zeilenumbrüche durch System-Zeilenumbrüche
        text = text.replaceAll("\\n", System.getProperty("line.separator"));
        JFileChooser fc = new JFileChooser();
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        String[] a = { "txt" };
        fc.setFileFilter(new FileNameExtensionFilter("Textdateien, .txt", a));
        int opt = fc.showSaveDialog(fenster);
        if (opt == JFileChooser.CANCEL_OPTION) {
            JOptionPane.showMessageDialog(fenster, "Keine Datei ausgewählt",
                    "Dateiauswahl-Abbruch", JOptionPane.ERROR_MESSAGE);
            return;
        }
        File f = fc.getSelectedFile();
        try {
            if (!f.exists())
                f.createNewFile();
            BufferedWriter writer = new BufferedWriter(new FileWriter(f));
            writer.write(text);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
