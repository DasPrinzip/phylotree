package graph;

/**
 * Created by marcel on 19.05.2017.
 */
public class Edge {
    
    private Node head; // child >
    private Node tail; // parent -
    private double weight = 1.0;

    public double getWeight() {
        return weight;
    }

    public Node getHead() {
        return head;
    }

    public Node getTail() {
        return tail;
    }

    public Edge(Node tail, Node head, double weight){
        this.tail = tail;
        this.head = head;
        this.weight = weight;
    }
}
