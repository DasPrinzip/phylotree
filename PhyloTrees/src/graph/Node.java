package graph;

import java.util.ArrayList;

/**
 * Created by marce on 19.05.2017.
 */
public class Node {

    private ArrayList<Edge> edges_out = new ArrayList<>();
    private Edge edge_in;
    private String name;
    private int layout_width = 1;

    public int getLayout_width() {
        return this.layout_width;
    }

    public ArrayList<Node> getChildren() {
        ArrayList<Node> resultList = new ArrayList<>();
        for (Edge k : edges_out) {
            resultList.add(k.getHead());
        }
        return resultList;
    }

    public Node getParent() {
        return edge_in.getTail();
    }

    public Node getChildByName(String name) {
        Node tempKnoten;
        for (Edge k : edges_out) {
            tempKnoten = k.getHead();
            if (tempKnoten.getName().equals(name)) {
                return tempKnoten;
            }
        }
        return null;
    }

    public String getName() {
        return this.name;
    }

    public Edge getEdge_in() {
        return this.edge_in;
    }

    public ArrayList<Edge> getEdges_out() {
        return edges_out;
    }

    public void setEdgeIn(Edge edge){
        this.edge_in = edge;
    }

    public void addEdgeOut(Edge edge){
        edges_out.add(edge);
        this.layout_width += edge.getHead().getLayout_width();
    }

    public Node(String name) {
        this.name = name;
    }
}
