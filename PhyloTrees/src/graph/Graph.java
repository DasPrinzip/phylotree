package graph;

import java.util.ArrayList;

/**
 * Created by marcel on 19.05.2017.
 */
public class Graph {

    private ArrayList<Node> nodeList = new ArrayList<>();
    private ArrayList<Edge> edgeList = new ArrayList<>();
    private ArrayList<Node> leaves = new ArrayList<>();
    private Node root = null;

    public Node getNodeByName(String name){
        for(Node n : nodeList){
            if(n.getName().equals(name)){
                return n;
            }
        }
        return null;
    }

    public String printASCIITree() {
        ArrayList<String> ASCIITree = new ArrayList<>();
        for (int i = 0; i < root.getLayout_width(); i++) {
            ASCIITree.add(i+"");
        }
        double maxWeight = getMaxWeight();
        calculateASCIITree(root, ASCIITree, -1,0, maxWeight);
        String result = "";
        for (String s : ASCIITree) {
            result += s + "\n";
        }

        //System.out.println(result);
        return result;
    }

    private int calculateASCIITree(Node node, ArrayList<String> ASCIITree, int offset_x, int offset_y, double maxWeight) {
        int node_position = offset_y;
        if (!node.getEdges_out().isEmpty()) {
            node_position = node.getChildren().get(0).getLayout_width() + offset_y;
        }

        String offset_String = "";
        for (int i = 0; i < offset_x; i++) {
            offset_String += " ";
        }
        int numberOfMinus = 0;
        if (node.getEdge_in() != null) {
            numberOfMinus = (int) ((node.getEdge_in().getWeight() / maxWeight) * 10);
            offset_String += "+-";
        }
        for (int i = 0; i < numberOfMinus -1; i++) {
            offset_String += "-";
        }
        ASCIITree.set(node_position, offset_String + node.getName());
        boolean positive_offset_y = false;

        int pos = 0;
        String temp = "";

        for (Edge edge : node.getEdges_out()) {
            if (positive_offset_y == false) {
                pos = calculateASCIITree(edge.getHead(), ASCIITree, offset_x + 1 + numberOfMinus,node_position - edge.getHead().getLayout_width(), maxWeight);
                positive_offset_y = true;
                for (int i = 1; i < node_position - pos; i++) {
                    temp = ASCIITree.get(node_position - i).substring(0, offset_x + numberOfMinus +1 ) + "!" + ASCIITree.get(node_position - i).substring(offset_x + numberOfMinus + 2);
                    ASCIITree.set(node_position - i, temp);
                }
            } else {
                pos = calculateASCIITree(edge.getHead(), ASCIITree, offset_x + 1 + numberOfMinus,node_position + 1, maxWeight);
                for (int i = 1; i < pos - node_position; i++) {
                    temp = ASCIITree.get(node_position + i).substring(0, offset_x + numberOfMinus +1 ) + "!" + ASCIITree.get(node_position + i).substring(offset_x + numberOfMinus + 2);
                    ASCIITree.set(node_position + i, temp);
                }
            }
        }
        return node_position;
    }

    private double getMaxWeight() {
        double maxWeight = 0.0;
        for (Edge edge : edgeList) {
            if (edge.getWeight() > maxWeight) {
                maxWeight = edge.getWeight();
            }
        }
        return maxWeight;
    }

    public ArrayList<Node> getLeaves() {
        return leaves;
    }

    public void addLeave(Node leave) {
        leaves.add(leave);
    }

    public Node getRoot() {
        return root;
    }

    public void setRoot(Node root) {
        this.root = root;
    }

    public ArrayList<Node> getNodes() {
        return nodeList;
    }

    public ArrayList<Edge> getEdges() {
        return edgeList;
    }

    public void addNode(Node k){
        nodeList.add(k);
    }

    public void addEdge(Edge k){
        edgeList.add(k);
    }

    public void printAnzNode(){
        System.out.println("Anzahl Node: " + nodeList.size());
    }

    public void printAnzEdges(){
        System.out.println("Anzahl Edges: " + edgeList.size());
    }

    public void printRoot(){
        System.out.println("Wurzel ist: " + root.getName());
    }

}
